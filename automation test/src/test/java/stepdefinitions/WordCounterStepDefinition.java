package stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import questions.CounterCharacters;
import questions.CounterWords;
import tasks.Go;
import tasks.WordCounter;


public class WordCounterStepDefinition {


    @Before
    public void setStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^the user want to open the counter page$")
    public void theUserWantToOpenTheCounterPage() {
        OnStage.theActorCalled("Actor").wasAbleTo(Go.wordCounter());
    }

    @When("^the user select the text into paragrafh (.*)$")
    public void theUserSelectTheText(String text) {
        OnStage.theActorInTheSpotlight().attemptsTo(WordCounter.in(text));
    }

    @Then("^it is verified that the words and character is the hope (.*) and (.*) characters$")
    public void itIsVerifiedThatTheWordsAndCharacterIsTheHope(Integer words, Integer characters) {
        String text = OnStage.theActorInTheSpotlight().recall("text");
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(
                        CounterWords.inText(text), Matchers.equalTo(words)
                ),
                GivenWhenThen.seeThat(
                        CounterCharacters.inText(text), Matchers.equalTo(characters)
                )

        );

    }


}
