package models;

public class WordDTO {

    private String word;
    private int concurrences;

    public WordDTO(String word, int concurrences) {
        this.word = word;
        this.concurrences = concurrences;
    }

    public String getWord() {
        return word;
    }

    public int getConcurrences() {
        return concurrences;
    }
}
