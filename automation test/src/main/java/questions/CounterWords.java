package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import utils.Utils;


public class CounterWords implements Question<Integer> {

    private String data;

    public CounterWords(String data) {
        this.data = data;
    }

    @Override
    public Integer answeredBy(Actor actor) {
        Utils counterHandler = new Utils();
        return counterHandler.countWordsByLine(data);

    }

    public static CounterWords inText(String text) {
        return new CounterWords(text);
    }
}
