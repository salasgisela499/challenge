package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import utils.Utils;

public class CounterCharacters implements Question<Integer> {

    private String data;

    public CounterCharacters(String data) {
        this.data = data;
    }

    @Override
    public Integer answeredBy(Actor actor) {
        Utils counterHandler = new Utils();
        return counterHandler.countCharactersByLine(data);
    }

    public static CounterCharacters inText(String text) {
        return new CounterCharacters(text);

    }
}
