package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static userinterface.WordCounter.TEXT;


public class WordCounter implements Task {

    String data;

    public WordCounter(String data) {
        this.data = data;
    }

    public static WordCounter in(String data) {
        return Tasks.instrumented(WordCounter.class, data);

    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.remember("text", TEXT.of(data).resolveFor(actor).getText());
    }
}
