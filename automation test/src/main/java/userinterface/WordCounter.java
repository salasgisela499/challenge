package userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;

public class WordCounter extends PageObject {

    public static final Target TEXT = Target.the("Text").locatedBy("(//p)[{0}]");

}
