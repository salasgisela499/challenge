import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Utils {

    private List<String> wordList;
    private List<WordDTO> wordDTOList;

    public  Utils(){
        this.wordList = new ArrayList<>();
        this.wordDTOList = new ArrayList<>();
    }

    public String getOrderedList(){

        String result = "";

        countWordsIndividually();

        wordDTOList.sort(Comparator.comparing(WordDTO::getConcurrences).reversed());

        for (WordDTO data: wordDTOList) {
            result += data.getWord() +": " + data.getConcurrences() + "\n";
        }

        return result;
    }


    private void countWordsIndividually(){

        wordList.sort(null);

        String word = null;

        for(int i = 0; i < wordList.size(); i++){

            int count = 1;
            word = wordList.get(i);

            for(int j = (i + 1); j < wordList.size(); j++){

                if(word.equalsIgnoreCase(wordList.get(j))){
                    count++;
                }else {
                    i = j-1;
                    break;
                }

                if(j == (wordList.size() - 1)){
                    i = j;
                }

            }
            wordDTOList.add(new WordDTO(word, count));
        }
    }

    private void fillWordList(String... words){
        wordList.addAll(Arrays.asList(words));
    }

    public int countWordsByLine(String text){

        String[] words = text.split("\\s");

        fillWordList(words);
        return words.length;
    }

    public int countCharactersByLine(String text){
        return text.trim().length();
    }

}
