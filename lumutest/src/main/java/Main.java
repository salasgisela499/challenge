import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    private static final String FILE_NAME = "src/main/resources/text.txt";

    public static void main(String [] args){

        Utils utils = new Utils();

        int totalWords = 0;
        int totalCharacters = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {

            String line;
            while ((line = br.readLine()) != null) {

                totalWords += utils.countWordsByLine(line);
                totalCharacters += utils.countCharactersByLine(line);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(" ");
        System.out.println("Characters = " + totalCharacters);
        System.out.println("Words = " + totalWords);
        System.out.println(" ");
        System.out.println(utils.getOrderedList());

    }
}
